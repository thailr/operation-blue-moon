#+AUTHOR: Vaibhav Kaushik
#+EMAIL: vaibhavkaushik@disroot.org
* GOALS
** Computer Network
*** [#A] Computer Network by Kruose & Ross [0%]
    - [ ] Chapter 6. Wireless and Mobile Network
    - [ ] Chapter 7. Multimedia Network
    - [ ] Chapter 8. Security in Computer Network
    - [ ] Chapter 9. Network Management
*** [[https://github.com/python/cpython/blob/master/Lib/socket.py][Python Socket module Source Code]]
** Data-Structures & Algorithms 
*** [#A] Practice Competitive Programming                              :code:
    SCHEDULED: <2018-09-25 Tue .2d/4d>
    :PROPERTIES:
    :STYLE:    habbit
    :END:
**** [[https://www.codechef.com/certification/prepare#foundation][Reference]]
***** Array
***** Strings
***** Stacks and Queues
***** Maths Operations
***** Euclid��s GCD Algorithm
***** Prime Numbers, divisibility of numbers
***** Basic Recursion
***** Sorting
***** Binary Search
***** Heaps (priority queue)
***** Disjoint Set Union
***** Trees
****** Lowest Common Ancestors
****** Binary Index Tree (Fenwick tree)
****** Segment Trees
***** Naive string searching
***** Greedy Algorithms
***** Dynamic programming
**** [[http://codeforces.com/blog/entry/57282][Blog]]                  :read:
*** Project Euler
** System
*** [#A] [[https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/ch01][Control Groups]]
**** [[https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/sec-relationships_between_subsystems_hierarchies_control_groups_and_tasks][How and Why Cgroup]]
**** [[https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/ch-using_control_groups][Using Cgroups]]
**** [[https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/ch-subsystems_and_tunable_parameters][Subsystems and Tunable Parameters]]
**** [[https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/control-group-application-examples][Applications examples]]
*** Python Scripting
**** [#A] [[https://automatetheboringstuff.com/][Automate Boring Stuffs]]                                       :read:
*** Awk
**** [[https://gregable.com/2010/09/why-you-should-know-just-little-awk.html][Why you learn just a little AWK]]
**** [[http://www.grymoire.com/Unix/Awk.html][Awk Tutorial]]
*** Network File System
** Database
*** PostgreSQL
** Development
*** [#A] [[https://pybit.es/pages/challenges.html][PyBites Challenges]]
*** [#B] Gophercise [0%]
  - [ ] Quiz Game
  - [ ] URL Shortener
  - [ ] Choose your Own Adventure
  - [ ] HTML Link Parser
  - [ ] Sitemap Builder
  - [ ] CLI Task Manager
  - [ ] Phone Number Normalizer
  - [ ] Deck Of Cards
  - [ ] Blackjack
  - [ ] File Renaming Tool
  - [ ] Quite HackerNews
  - [ ] Recover Middleware
  - [ ] Twitter Retweet
  - [ ] Secret API and CLI
  - [ ] Image Transform Service
  - [ ] Building Images
  - [ ] Building PDFs
*** Testing
** Web Development
*** [#A] Django
**** [[https://developer.mozilla.org/en-US/docs/Learn/Server-side/First_steps][Server-side website programming first steps]]
**** [[https://docs.djangoproject.com/en/2.1/intro/][Django Project]]
**** [[https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django][MDN Django Docs]]
*** Golang
**** Golang Series [0%]
   - [ ] Go Bootcamp Series
   - [ ] Go Web Applications
   - [ ] [[https://www.calhoun.io/series][Calhoun Series]]
*** [#A] Create a Web-app
**** [#A] [[https://github.com/vaibhavk/devhelp][DevHelp]]
** Security
*** SELinux
*** OWASP and Security Practices
** Organizations
*** Honeynet
** Projects
*** [[https://github.com/github/hub][github/hub]]
*** [[https://github.com/ipfs/go-ipfs][go-ipfs]]
*** [[https://github.com/taskcluster][TaskCluster]]
* PLAN
